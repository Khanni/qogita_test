/*
  Integration test using Cypress to test the Product page component
 */
describe('Integration test', () => {
    beforeEach(() => {
        cy.server()
        cy.visit('/');
    });
    it('render home page - verify number of products on the home page and api is being called on a button click', () => {
        cy.findByTestId('product-list').should('exist');
        cy.findAllByTestId('product-card').should('exist').should('have.length', 20);
        cy.findByRole('pagination').should('exist');
        cy.findByText('Next').filter('button').should('exist');
        cy.findByText('Prev').filter('button').should('exist');
        cy.findByText('2').click();
        cy.request('/api/products').then((res) => {
            expect(res.status).equal(200);
        })
    })
})