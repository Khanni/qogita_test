export type Product = {
    name: string;
    gtin: string;
    recommendedRetailPrice: number;
    recommendedRetailPriceCurrency: string;
    imageUrl: string;
    brandName: string;
    categoryName: string;
};

/**
 * The response type of errors from /api/*.
 */
export type ErrorResponse = string;

/**
 * The response type of /api/products
 */
export type ProductsResponse = {
    count: number;
    page: number;
    results: Product[];
};

/**
 * The response type of /api/products/[gtin].
 */
export type ProductResponse = Product;


export interface ActionTypes {
    type: 'ADD_TO_CART',
    payload: CartItemsMap
}

export interface ProductTypes {
    name: string;
    gtin: string;
    recommendedRetailPrice: number;
    recommendedRetailPriceCurrency: string;
    imageUrl: string;
    brandName: string;
    categoryName: string;
    noOfProducts: number
}

/**
 *Types of cart items data*.
 */

interface CartItemsMap {
    [key: string]: ProductTypes
}


/**
 *Types of context state*.
 */
export interface ContextState {
    cartItems: CartItemsMap
}

export interface ContextType {
    cartItemsState: ContextState,
    addCartItem: React.Dispatch<ActionTypes>
}