import type { AppProps } from 'next/app';
import GlobalContextProvider from "../components/context/globalContextProvider";
import '../global.css';

const QogitaApp = ({ Component, pageProps }: AppProps): JSX.Element => (
    <GlobalContextProvider>
  <Component {...pageProps} />
    </GlobalContextProvider>
);

export default QogitaApp;
