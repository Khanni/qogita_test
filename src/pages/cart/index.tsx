import React from "react";
import Layout from '../../components/Layout';
import AmountSummary from '../../components/AmountSummary';
import CartItem from "../../components/CartItem";

const CartPage = () => (
    <Layout>
        <div className=" w-full flex">
            <div className="flex-1 flex-col">
                <div className="bg-white border border-white shadow-lg  rounded-3xl p-4 m-4">
                    <h4 className="mb-16 text-xl font-semibold tracking-tight text-gray-800">My Cart</h4>
                    <CartItem/>
                </div>
            </div>
            <AmountSummary/>
        </div>
    </Layout>
);

export default CartPage;
