import React, {useEffect, useState} from "react";
import Layout from '../components/Layout';
import ProductCard from "../components/ProductCard";
import {ProductsResponse} from "../types";
import {fetchProductList} from "../components/api";
import Pagination from "../components/Pagination";


const HomePage = () => {
    const [productList, setProductList] = useState<ProductsResponse>();
    const [pageNumber, setPageNumber] = useState<number>(1)
    useEffect(() => {
        fetchProductList(setProductList, pageNumber)
    }, [])

    return (
        <Layout>
            {
                productList && productList.results && productList.results.length > 0 ? (
                    <div data-testid="product-list">
                        <div
                            className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3 gap-2">
                            {
                                productList.results.map((item) => {
                                    return (
                                        <div key={`product${item.gtin}`}>
                                            <ProductCard {...item} />
                                        </div>
                                    )
                                })
                            }
                        </div>
                        <Pagination pageCount={productList.count} setPageNumber={setPageNumber}
                                    setProductList={setProductList} pageNumber={pageNumber}/>
                    </div>
                ) : (<div>No item to show</div>)
            }
        </Layout>
    )
}
export default HomePage;
