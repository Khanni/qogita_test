import React from 'react';
import {fetchProductList} from "../api";
import {ProductsResponse} from "../../types";


const pageSize = 20 // default page size for products per page

const Pagination = (props: { pageCount: number, setPageNumber: (pageNumber: number) => void, setProductList: (productList: ProductsResponse) => void, pageNumber: number }) => {
    const totalPages = props.pageCount / pageSize; // get the total number of pages for pagination bar.
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(props.pageCount / pageSize); i++) {
        pageNumbers.push(i);
    }

    const handlePrev = () => {
        const prePage = props.pageNumber > 1 ? props.pageNumber - 1 : 1;
        fetchProductList(props.setProductList, prePage)
        props.setPageNumber(prePage)
    }
    const handleNext = () => {
        const nextPage = props.pageNumber < totalPages ? (props.pageNumber + 1) : totalPages;
        fetchProductList(props.setProductList, nextPage)
        props.setPageNumber(nextPage)
    }

    const handleClick = (event: any) => {
        fetchProductList(props.setProductList, event.target.id)
        props.setPageNumber(event.target.id)
    }
    const renderPageNumbers = pageNumbers.map(number => {
        return (
            <li key={`page-${number}`}>
                <button
                    className={`h-10 px-5 transition-colors duration-150 ${number === props.pageNumber ? 'bg-indigo-600 text-white' : 'bg-white'}focus:shadow-outline`}
                    key={number} id={`${number}`} onClick={handleClick}>{number}
                </button>
            </li>
        );
    });
    return (
        <nav className="relative z-0 inline-flex rounded-md shadow-sm -space-x-px mt-10 float-right mb-16"
             role="pagination">
            <ul className="inline-flex">
                <li>
                    <button onClick={handlePrev}
                            className="h-10 px-5 text-indigo-600 transition-colors duration-150 bg-white rounded-l-lg focus:shadow-outline hover:bg-indigo-100">Prev
                    </button>
                </li>
                {renderPageNumbers}
                <li>
                    <button onClick={handleNext}
                            className="h-10 px-5 text-indigo-600 transition-colors duration-150 bg-white rounded-r-lg focus:shadow-outline hover:bg-indigo-100">Next
                    </button>
                </li>
            </ul>
        </nav>
    )
}

export default Pagination