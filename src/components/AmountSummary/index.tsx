import React, {useContext} from "react";
import {GlobalContext} from "../context/globalContextProvider";

const AmountSummary = () => {
    const cartContext = useContext(GlobalContext);
    const cartItems = cartContext.cartItemsState.cartItems

    const TotalAmount = Object.values(cartItems).length > 0 ? Object.values(cartItems).reduce((r, {
        recommendedRetailPrice,
        noOfProducts
    }) => r + recommendedRetailPrice * noOfProducts, 0) : 0.00;
    const TotalAmountWithTwoDeci = Math.round(TotalAmount * 100) / 100
    return (
        <div className="max-w-xs overflow-hidden rounded-lg shadow-lg">
            <div className="px-6 py-4">
                <h4 className="mb-3 text-xl font-semibold tracking-tight text-gray-800">Amount summary</h4>
                <div className="flex">
                    <div className="flex-1">
                        <p className="leading-normal text-gray-700">Total amount</p>
                    </div>
                    <p className="leading-normal text-gray-700">{TotalAmountWithTwoDeci} Euro</p>
                </div>
                <div className="flex mt-5 border-b pb-12">
                    <div className="flex-1">
                        <p className="leading-normal text-gray-700">Shipping</p>
                    </div>
                    <p className="leading-normal text-gray-700">0 Euro</p>
                </div>
                <div className="flex mt-5 font-black">
                    <div className="flex-1">
                        <p className="leading-normal text-gray-700">Total cost (incl. VAT)</p>
                    </div>
                    <p className="leading-normal text-gray-700 pl-10">{TotalAmountWithTwoDeci} Euro</p>
                </div>
            </div>
        </div>
    )
}

export default AmountSummary