import React, {useReducer} from "react";
import {ContextState, ActionTypes, ContextType} from "../../types";


const initialState: ContextState = {
    cartItems: {}
}

export const GlobalContext = React.createContext<ContextType>({
    cartItemsState: initialState,
    addCartItem: () => {
    }
})


function cartReducer(state: ContextState, action: ActionTypes) {
    switch (action.type) {
        case 'ADD_TO_CART':
            return {
                ...state,
                cartItems: action.payload
            }

        default:
            return
    }
}

type Props = {
    children: React.ReactNode;
};
const GlobalContextProvider = ({children}: Props) => {
    // @ts-ignore
    const [cartItemsState, addCartItem] = useReducer(cartReducer, initialState)

    const contextValue = {
        cartItemsState,
        addCartItem
    }
    return (
        <GlobalContext.Provider value={contextValue}>
            {children}
        </GlobalContext.Provider>
    )
}

export default GlobalContextProvider