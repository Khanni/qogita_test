import React from 'react';
import {render} from '@testing-library/react';
import ProductCard from '../../ProductCard'


describe('product card', () => {
    test('render product cart with One product', () => {
        const mockedProduct = {
            "name": "Shiseido Synchro Skin Self-refreshing Concealer Concealer 1 Pc.",
            "gtin": "0730852157347",
            "recommendedRetailPrice": 38.99,
            "recommendedRetailPriceCurrency": "EUR",
            "imageUrl": "https://images.qogita.com/files/images/variants/HqhfTE5PfAZRfHJzCSNnMA.jpg",
            "brandName": "Shiseido",
            "categoryName": "Concealer"
        }
        const {getByText} = render(<ProductCard {...mockedProduct}/>);
        expect(getByText(mockedProduct.brandName)).toBeInTheDocument();
        expect(getByText(mockedProduct.name)).toBeInTheDocument();
        expect(getByText(mockedProduct.recommendedRetailPrice)).toBeInTheDocument();
        expect(getByText(mockedProduct.recommendedRetailPriceCurrency)).toBeInTheDocument();
        expect(getByText(mockedProduct.categoryName)).toBeInTheDocument();
        expect(getByText('BUY NOW')).toBeInTheDocument();
    });
}) 