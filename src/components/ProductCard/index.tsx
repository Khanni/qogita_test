import React, {useContext} from "react";
import {Product} from "../../types";
import {GlobalContext} from "../context/globalContextProvider";

const ProductCard = (product: Product) => {
    const cartContext = useContext(GlobalContext);
    const cartItems = cartContext.cartItemsState.cartItems

    const AddItemToCart = () => {
        if (Object.keys(cartItems).indexOf(product.gtin) === -1) {
            cartItems[product.gtin] = {
                noOfProducts: 1,
                ...product
            };
            cartContext.addCartItem({
                type: 'ADD_TO_CART',
                payload: cartContext.cartItemsState.cartItems
            })
        } else {
            const copyOfCartItems = {...cartItems};
            const value = (copyOfCartItems[product.gtin].noOfProducts + 1)
            copyOfCartItems[product.gtin] = {
                ...product,
                noOfProducts: value
            };
            cartContext.addCartItem({
                type: 'ADD_TO_CART',
                payload: copyOfCartItems
            })
        }
    }


    return (
        <div className="rounded overflow-hidden shadow-lg" data-testid="product-card">
            <img src={product.imageUrl} alt={`${product.categoryName}_image`}/>
            <div className="px-6 py-4 border-b">
                <div className="max-w-3xl w-full">
                    <div className="font-bold text-xl mb-2">
                        {product.categoryName}
                    </div>
                    <span
                        className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">{product.brandName}</span>
                </div>
                <p className="text-gray-700 text-base">
                    {product.name}
                </p>
                <div className="mt-3 flex items-center">
                           <span
                               className="font-bold text-xl">{product.recommendedRetailPrice}</span>&nbsp;<span
                    className="text-sm font-semibold">{product.recommendedRetailPriceCurrency}</span>
                </div>
            </div>
            <button

                type="button"
                onClick={AddItemToCart}
                className="bg-indigo-400 opacity-75 hover:opacity-100 text-black hover:text-gray-900 m-4 px-10 py-2 font-semibold">
                <i className="mdi mdi-cart -ml-2 mr-2"/> BUY NOW
            </button>
        </div>

    )
}
export default ProductCard



