import {ProductsResponse} from "../types";

export const fetchProductList = (setProductList: (productList: ProductsResponse) => void, pageNumber: number) => {
    fetch(`/api/products?page=${pageNumber}`).then((response) => response.json()).then((list) => {
        setProductList(list)
    }).catch(() => console.log('error'))
}