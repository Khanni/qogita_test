import React, {useContext} from "react";
import Link from "next/link";
import {GlobalContext} from "../context/globalContextProvider";

const Header = () => {
    const cartContext = useContext(GlobalContext);
    return (
        <nav className="p-4 rounded bg-indigo-400 shadow flex flex-wrap sm:flex-nowrap justify-between">
            <Link href="/">
                <div className="flex items-center flex-no-shrink text-white mr-6 cursor-pointer">
                    <svg className="h-8 w-8 mr-2" width="54" height="54" viewBox="0 0 54 54"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M13.5 22.1c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05zM0 38.3c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05z"/>
                    </svg>
                    <span className="font-semibold text-xl tracking-tight text-black">Qogita</span>
                </div>
            </Link>
            <div className="justify-self-end">
                <ul className="flex gap-4">
                    <li>
                        <Link href="/">
                            <a className="underline">Products</a>
                        </Link>
                    </li>
                    <li>
                        <Link href="/cart">
                           <span className="relative inline-block cursor-pointer">
                             <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24"
                                  stroke="currentColor">
                                   <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                         d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"/>
                             </svg>
                         <span
                             className="absolute top-0 right-0 inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 transform translate-x-1/2 -translate-y-1/2 bg-red-600 rounded-full">{Object.keys(cartContext.cartItemsState.cartItems).length}</span>
                        </span>
                        </Link>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default Header