import Header from './Header'

type Props = {
    children: React.ReactNode;
};

const Layout = ({children}: Props) => (
    <div className="container mx-auto px-4">
        <div className="">
            <Header/>
        </div>
        <div className="p-14">
            {children}
        </div>
    </div>
);

export default Layout;
