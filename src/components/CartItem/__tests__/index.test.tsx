import React from 'react';
import {render} from '@testing-library/react';
import CartItem from '../../CartItem';
import {GlobalContext} from "../../context/globalContextProvider";


describe('render cart component with multiple products - context testing', () => {
    const mockedCartItems = {
        cartItems: {
            5054563079435: {
                brandName: "Parodontax",
                categoryName: "Toothpaste",
                gtin: "5054563079435",
                imageUrl: "https://images.qogita.com/files/images/variants/aB9r5isuPDUTTD3nLNsXvQ.jpg",
                name: "Parodontax Duplo Herbal Fresh 75ml",
                noOfProducts: 1,
                recommendedRetailPrice: 29.99,
                recommendedRetailPriceCurrency: "EUR",
            },
            8411047151242: {
                brandName: "Instituto Espanol",
                categoryName: "Men's Perfume",
                gtin: "8411047151242",
                imageUrl: "https://images.qogita.com/files/images/variants/co8e7Y9gf272e2W2LgA6fj.jpg",
                name: "Poseidon The Black Men Edt Vapo 150 Ml - Beauty & Health",
                noOfProducts: 1,
                recommendedRetailPrice: 22.99,
                recommendedRetailPriceCurrency: "EUR",
            }
        }
    }

    const mockedContextValue = {
        cartItemsState: mockedCartItems,
        addCartItem: jest.fn()
    }

    test('render no item message when there is no cart item ', () => {
        const {getByText} = render(<CartItem/>);
        expect(getByText('No item is added')).toBeInTheDocument()

    });
    test('render cart items - verifying that cart has the correct number of products ', () => {
        const {getAllByTestId} = render(
            <GlobalContext.Provider value={mockedContextValue}>
                <CartItem/>
            </GlobalContext.Provider>
        );
        expect(getAllByTestId('cartItem')).toHaveLength(2);
    });
}) 