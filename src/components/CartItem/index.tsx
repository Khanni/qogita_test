import React, {useContext} from "react";
import {GlobalContext} from "../context/globalContextProvider";
import {ProductTypes} from "../../types"


const CartItem = () => {
    const cartContext = useContext(GlobalContext);
    const cartItems = cartContext.cartItemsState.cartItems;
    const removeItemFromCart = (cartId: string) => {
        const copyOfCartItems = {...cartItems};
        delete copyOfCartItems[cartId];
        cartContext.addCartItem({
            type: 'ADD_TO_CART',
            payload: copyOfCartItems
        })
    };

    const updateProductQuantity = (e: any, product: ProductTypes) => {
        const copyOfCartItems = {...cartItems};
        const value = e.target.value
        copyOfCartItems[product.gtin] = {
            ...product,
            noOfProducts: value
        };
        cartContext.addCartItem({
            type: 'ADD_TO_CART',
            payload: copyOfCartItems
        })
    }

    return (
        <div>
            {Object.keys(cartItems).length > 0 ? (
                Object.keys(cartItems).map(key => {
                    const cartItem = cartItems[key];
                    return (
                        <div
                            className={`flex-none sm:flex ${Object.keys(cartItems).length > 0 ? 'border-b mb-14' : null}`}
                            data-testid="cartItem" key={`cartItem${cartItem.gtin}`}>
                            <div className=" relative h-32 w-32   sm:mb-0 mb-3">
                                <img src={cartItem.imageUrl} alt={`${cartItem.categoryName}_image`}
                                     className=" w-32 h-32 object-cover rounded-2xl"/>
                            </div>
                            <div className="flex-auto sm:ml-5 justify-evenly">
                                <div className="sm:mt-2">
                                    <div className="flex flex-col">
                                        <div
                                            className="w-full flex-none text-lg text-gray-800 font-bold leading-none">{cartItem.categoryName}</div>
                                        <div className="w-full flex">
                                            <div className="flex-1 text-gray-500 my-1">
                                                <span className="mr-3 ">{cartItem.name}</span>
                                            </div>
                                            <div className="mr-0">
                                                <input type="number"
                                                       className="appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                                       id={key} value={cartItem.noOfProducts}
                                                       onChange={(e) => updateProductQuantity(e, cartItem)}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex pt-2  text-sm text-gray-500 mt-12 mb-6">
                                    <button className="flex-1 inline-flex items-center"
                                            onClick={() => removeItemFromCart(cartItem.gtin)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none"
                                             viewBox="0 0 24 24" stroke="currentColor">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                                  d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                                        </svg>
                                        <p>Remove</p>
                                    </button>

                                    <h4 className="flex-no-shrink px-5 ml-4 py-2 font-black">{cartItem.recommendedRetailPrice}{cartItem.recommendedRetailPriceCurrency}</h4>
                                </div>
                            </div>
                        </div>
                    )
                })
            ) : (
                <div className="flex items-center mb-4 rounded-b bg-blue-500 text-white text-sm font-bold px-4 py-3"
                     role="alert">
                    <svg className="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path
                            d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z"/>
                    </svg>
                    <p> No item is added.</p>
                </div>
            )}
        </div>
    )
}

export default CartItem